package Eljocdelavida;

import java.util.Random;

import java.util.Scanner;

/**
 * Buscamines
 * @version 07-03-2022
 * @author Jesús Arboledas
 * @since 07-03-2022
 */

public class Eljocdelavida {

	static char opcio;
	static int mida;
	static boolean opcioA = false;
	static char mapa[][];
	static int mapa2[][];

	/**
	 * El programa consiste en replicar el juego de la vida de John Horton Conway en 1970
	 */
	/**
	 * En esta funcion se ejecutan las principales funciones 
	 * @param args String
	 */
	public static void main(String[] args) {
		
		PresentaTitols();
		do {
			PresentaMenu();
			opcio = DemanaCaracter();
			Executa(opcio);
		} while (opcio != 'd');
	}
	/**
	 * En esta funcion se muestra un titulo de bienvenida al programa 
	 */
	static void PresentaTitols() {
		
		System.out.print("Benvingut al Joc de la vida");
	}
	/**
	 * En esta funcion se muestra el menú principal del programa
	 */
	static void PresentaMenu() {
		
		System.out.println();
		System.out.println("a) Incialitza tauler");
		System.out.println("b) Visualitzar tauler");
		System.out.println("c) Iterar");
		System.out.println("d) Sortir");
	}
	/**
	 * En esta funcion captura la opción del menú principal al usuario 
	 */

	static char DemanaCaracter() {
		
		Scanner lector = new Scanner(System.in);
		boolean correcte = false;
		char caracterEntrado = 0;
		while (correcte == false) {
			System.out.println("Introdueix una opci�: ");
			caracterEntrado = lector.next().charAt(0);
			if (caracterEntrado != 'a' && caracterEntrado != 'b' && caracterEntrado != 'c' && caracterEntrado != 'd') {
				System.out.println("Error: No has introduit cap de les opcions donades");
				lector.nextLine();
			} else
				correcte = true;
		}
		return caracterEntrado;
	}
	/**
	 * En esta funcion se ejecuta la opcion elegida
	 * @param opcio utilitza la opcio triada per l'usuari del menu principal
	 */

	static void Executa(char opcio) {

		switch (opcio) {
		case 'a':
			IniciarMatriu();
			break;
		case 'b':
			VeureMatriu();
			break;
		case 'c':
			Iterar();
			break;
		case 'd':
			Adeu();
		}

	}
	/**
	 * En esta funcion inicia la matriz, que representa el tablero 
	 */
	static void IniciarMatriu() {
		boolean correcte2 = false;
		Scanner lector = new Scanner(System.in);
		Random random = new Random();
		while (!correcte2) {
			System.out.println("Introdueix la mida del tauler (4-10):");
			mida = lector.nextInt();
			if (mida <= 3 || mida >= 11) {
				System.out.println("Error: La mida ha de ser entre 4 i 10!");
			} else {
				mapa = new char[mida][mida];
				correcte2 = true;
				opcioA = true;
				for (int i = 0; i < mapa.length; i++) {
					for (int j = 0; j < mapa[i].length; j++) {
						mapa[i][j] = '-';
					}
				}
				int totalcaselles = mida * mida;
				int Rang1 = totalcaselles / 2;
				int Rang2 = totalcaselles / 4;
				int vives = (int) Math.floor(Math.random() * (Rang1 - (Rang2)) + (Rang2));
				System.out.println("vives: " + vives);
				for (int i = 0; i < vives; i++) {
					boolean posiciook = false;
					do {
						int random1 = random.nextInt(mida);
						int random2 = random.nextInt(mida);
						if (mapa[random1][random2] != '+') {
							mapa[random1][random2] = '+';
							posiciook = true;
						}
					} while (posiciook == false);
				}

			}
			lector.nextLine();
		}
	}
	/**
	 * En esta funcion muestra la matriz, es decir el estado del tablero
	 */
	static void VeureMatriu() {
		if (!opcioA) {
			System.out.println("No s'ha inicialitzat el tauler, passa primer per l'opció a)");
		} else {
			for (int i = 0; i < mapa.length; i++) {
				for (int j = 0; j < mapa[i].length; j++) {
					System.out.print(mapa[i][j] + "\t");
				}
				System.out.println();
			}
		}
	}
	/**
	 * En esta funcion itera el tablero para que vaya evolucionando automaticamente 
	 */
	static void Iterar() {
		Scanner Lector = new Scanner(System.in);
		boolean correcte = false;
		boolean iterarok = false;
		int iteracions = 0;
		if (!opcioA) {
			System.out.println("No s'ha inicialitzat el tauler, passa primer per l'opció a)");
		} else {
			for (int i = 0; i < mapa.length; i++) {
				for (int j = 0; j < mapa[i].length; j++) {
					System.out.print(mapa[i][j] + "\t");
				}
				System.out.println();
			}
			while (!correcte) {
				try {
					do {
						System.out.println("Quantes iteracions vols fer?");
						iteracions = Lector.nextInt();
						if (iteracions > 100 || iteracions < 0) {
							System.out.println("El número d'iteracions ha de ser entre 0 i 100!");
						} else
							iterarok = true;
					} while (!iterarok);
					correcte = true;
				} catch (Exception e) {
					System.out.println("No esta perm�s el valor introduit!");
					Lector.nextLine();
				}
			}

			int veins = 0;
			mapa2 = new int[mida][mida];
			for (int x = 0; x < iteracions; x++) {
				for (int i = 0; i < mapa.length; i++) {
					for (int j = 0; j < mapa[i].length; j++) {
						veins = 0;

						if (i - 1 >= 0 && j - 1 >= 0 && mapa[i - 1][j - 1] == '+') {
							veins++;
						}

						if (i - 1 >= 0 && mapa[i - 1][j] == '+') {
							veins++;
						}
						if (i - 1 >= 0 && j + 1 < mapa[i].length && mapa[i - 1][j + 1] == '+') {
							veins++;
						}
						if (j - 1 >= 0 && mapa[i][j - 1] == '+') {
							veins++;
						}
						if (j + 1 < mapa[i].length && mapa[i][j + 1] == '+') {
							veins++;
						}
						if (i + 1 < mapa.length && j - 1 >= 0 && mapa[i + 1][j - 1] == '+') {
							veins++;
						}
						if (i + 1 < mapa.length && mapa[i + 1][j] == '+') {
							veins++;
						}
						if (i + 1 < mapa.length && j + 1 < mapa[i].length && mapa[i + 1][j + 1] == '+') {
							veins++;
						}

						mapa2[i][j] = veins;
					}
				}

				for (int i = 0; i < mapa2.length; i++) {
					for (int j = 0; j < mapa2[i].length; j++) {
						if (mapa2[i][j] == 3 && mapa[i][j] == '-') {
							mapa[i][j] = '+';
						}
						if (mapa[i][j] == '+' && (mapa2[i][j] <= 1 || mapa2[i][j] >= 4)) {
							mapa[i][j] = '-';
						}
						System.out.print(mapa[i][j] + "\t");
					}
				}
			}
		}
	}
	/**
	 * En esta funcion se despide del usuario y acaba el programa
	 */
	static void Adeu() {
		System.out.println("Ad�u!");
	}
}
